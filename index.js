const express = require('express');
const bodyParser = require('body-parser');
const moment = require('moment');

const app = express();
const PORT = process.env.PORT || 56201;

// Middleware for parsing request body
app.use(bodyParser.text());

// Endpoint for squaring a number
app.post('/square', (req, res) => {
    const number = parseFloat(req.body);
    const square = Math.pow(number, 2);
    res.setHeader('Content-Type', 'application/json');
    res.status(200).json({ number: number, square: square });
});

// Endpoint for reversing text
app.post('/reverse', (req, res) => {
    const reversedText = req.body.split('').reverse().join('');
    res.setHeader('Content-Type', 'text/plain');
    res.status(200).send(reversedText);
});

// Endpoint for date information
app.get('/date/:year/:month/:day', (req, res) => {
    const { year, month, day } = req.params;
    const requestedDate = moment(`${year}-${month}-${day}`, 'YYYY-MM-DD');
    const currentDate = moment();
    const weekDay = requestedDate.format('dddd');
    const isLeapYear = requestedDate.isLeapYear();
    let difference = Math.abs(requestedDate.diff(currentDate, 'days'));
    if (requestedDate.isAfter(currentDate)) {
        difference += 1;
    }
    res.setHeader('Content-Type', 'application/json');
    res.status(200).json({ weekDay: weekDay, isLeapYear: isLeapYear, difference: difference });
});

// Start the server
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

